import cv2
import matplotlib.pyplot as plt
from CourseHeader.utils.FaceDetector import detect_face

# draw the coordinate of an image; make it easier to know the pixel position
def coordinates(file):
    img = plt.imread(file)
    plt.imshow(img)
    plt.show()


# draw a rectangle on an image;
'''
    img:    input image                             [img]
    start:  top left corner of the rectangle        [x1, y1]
    end:    bottom right corner of the rectangle    [x2, y2]
    color:  BGR value                               [B, G, R]
    thick:  thickness of the line                   -1 or possitive
'''
def rect(img, start, end, color, thick):
    cv2.rectangle(img, start, end, color, thick)
    return img


# draw texts on any position of an image
'''
    img:        input image                             [img]
    org:        bottom right corner of the text box     [x, y]
    fontScale:  font size                               1,2 ...
    color:      BGR value                               [B, G, R]
    thickness:  thickness of the text                   
    flip:       flip the text up side down              True or False
'''
def texts(img, text, org, fontScale, color, thickness, flip ):
    font = cv2.FONT_HERSHEY_SIMPLEX
    lineType = cv2.LINE_AA
    cv2.putText(img, text, org, font, fontScale, color, thickness, lineType, flip)
    return img


# detect locations of four corners for a face bounding box
'''
    return an list of four location parameters:
    1. the left-end at x axis;
    2. the right-end at x axis;
    3. the top at y axis;
    4. the bottom at y axis
'''
def getFace(frame):
    return detect_face(frame)


# OpenCV WaitKey to add delay for HighGUI events; return True or False
def quit():
    if cv2.WaitKey(1) == ord('q'):
        return True
    else:
        return False
