from BlockOpenCv import coordinates, rect, texts, getFace, quit
import numpy as np
import cv2
import time

img = cv2.imread('onepiece.jpeg')
info = np.iinfo(img.dtype)
print(info)
cv2.imshow('this', img)
time.sleep(10)

start = (10, 10)
end = (40,40)
color = [0, 255, 0]

resu = rect(img, start, end, color, 1)
cv2.imshow('that', resu)
time.sleep(10)